/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Mathieu Maneh
 */
public class ProduitAchete implements Serializable{
    private int quantité;
    private double remise;
    private Produit produit ;
    
    public ProduitAchete() {
        this.quantité = 0;
        this.remise = 0;
        this.produit = null;
    }
    
    public ProduitAchete(int quantité, double remise,Produit produit) {
        this.quantité = quantité;
        this.remise = remise;
        this.produit = produit;
    }
    
    public int getQuantité() {
        return quantité;
    }

    public void setQuantité(int quantité) {
        this.quantité = quantité;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    
   public double getPrixTotal(){
       return this.quantité *  this.produit.getPrixUnitaire();
   } 

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "quantit\u00e9=" + quantité + ", remise=" + remise + ", produit=" + produit + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.quantité;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.produit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantité != other.quantité) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.produit, other.produit)) {
            return false;
        }
        return true;
    }
    
   
    
}
