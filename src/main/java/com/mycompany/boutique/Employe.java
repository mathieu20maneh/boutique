/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Mathieu Maneh
 */
public class Employe extends Personne implements Serializable{
    private String cnss;
    private LocalDate dateEmbauche;
    
     public Employe() {
        super();
        this.cnss = null;
        this.dateEmbauche = LocalDate.parse((CharSequence) new SimpleDateFormat("dd-MM-yy")) ;
    }
     
    public Employe(String cnss, LocalDate dateEmbauche, long id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public String toString() {
        return "Employee "+ super.toString()+ "cnss=" + cnss + ", dateEmbauche=" + dateEmbauche + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.cnss);
        hash = 97 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {
            return false;
        }
        if (!Objects.equals(this.dateEmbauche, other.dateEmbauche)) {
            return false;
        }
        return true;
    }
    

}
