/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Mathieu Maneh
 */
public class Achat implements Serializable {
    private long id;
    private double remise;
    private LocalDate dateAchat; 
    private Personne acheteur ;
    private ArrayList<ProduitAchete> ListeProduits = new ArrayList<>();
    
    public Achat(){
        this.id = 0;
        this.remise = 0;
        this.dateAchat = LocalDate.now();
        this.ListeProduits = null;
        this.acheteur = null;
    }
    public Achat(long id, double remise, LocalDate dateAchat,ArrayList<ProduitAchete> ListeProduits, Personne acheteur) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.ListeProduits = ListeProduits;
        this.acheteur = acheteur;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Personne getAcheteur() {
        return acheteur;
    }

    public void setAcheteur(Personne acheteur) {
        this.acheteur = acheteur;
    }
    

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }
    
       public ArrayList<ProduitAchete> getListeProduits() {
        return ListeProduits;
    }

    public void setListeProduits(ArrayList<ProduitAchete> ListeProduits) {
        this.ListeProduits = ListeProduits;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", acheteur=" + acheteur + ", ListeProduits=" + ListeProduits + '}';
    }

    
    public double getRemiseTotale()
    {
        double RemiseTotale = 0;
        for(int i=0;i<ListeProduits.size();i++){
            RemiseTotale += ListeProduits.get(i).getRemise();
        }
        return RemiseTotale + this.remise;
            
        }
    public double getPrixTotale()
    {
        double PrixTotale = 0;
        for(int i=0;i<ListeProduits.size();i++){
            PrixTotale += ListeProduits.get(i).getPrixTotal();
        }
        return  PrixTotale-this.getRemiseTotale();
            
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.dateAchat);
        hash = 17 * hash + Objects.hashCode(this.acheteur);
        hash = 17 * hash + Objects.hashCode(this.ListeProduits);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.dateAchat, other.dateAchat)) {
            return false;
        }
        if (!Objects.equals(this.acheteur, other.acheteur)) {
            return false;
        }
        if (!Objects.equals(this.ListeProduits, other.ListeProduits)) {
            return false;
        }
        return true;
    }
    
}
